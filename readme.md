# Русифицированные дополнения для RInform

Этот репозиторий содержит набор русифицированных пользовательских библиотек (расширений) для платформы [RInform](https://rinform.org).

## Расширения

### howtoplayrus  
Краткое руководство по игре в интерактивную литературу (использован материал из ["A Beginner's Guide to Playing Interactive Fiction"](http://IFGuide.ramsberg.net)). Содержит информацию по перемещению, взаимодействию с игровыми объектами, персонажами, основные команды и многое другое.

Расширение добавляет в игру команды ПОМОЩЬ, СПРАВКА, ИНСТРУКЦИИ, которые выводят на экран данное руководство.

Может использоваться вместе с библиотекой [DMenus](http://mirror.ifarchive.org/if-archive/infocom/compilers/inform6/library/contributions/DMenus.zip) или [аналогичной](http://inform-fiction.org/extensions/menus.html). Если была подключена библиотека DMenus (или аналогичная) появится меню, содержащее список разделов данного руководства. В противном случае на экран будет выведен полный текст руководства.

* **[Скачать howtoplayrus](https://gitlab.com/johnbrown90210/rinformaddons/raw/master/library/howtoplayrus.h?inline=false)**
* [Посмотреть код](https://gitlab.com/johnbrown90210/rinformaddons/blob/master/library/howtoplayrus.h)

***

## Установка

Инструкции по установке указаны в заголовках исходных файлов соответствующих дополнений.

## Лицензии

Лицензии указаны в заголовках исходных файлов соответствующих дополнений.

## Обратная связь

Вы можете помочь проекту, сообщив об ошибке в работе дополнения или переводе. Это можно сделать следующими способами:  
* Создать [merge-request](https://gitlab.com/johnbrown90210/rinformaddons/merge_requests/new)
* Создать [issue](https://gitlab.com/johnbrown90210/rinformaddons/issues/new)
* Написать мне личное сообщение (ник *johnbrown*) или оставить сообщение в [тематической ветке](https://forum.ifiction.ru/viewtopic.php?id=2489) на форуме [iFiction.ru](https://forum.ifiction.ru)